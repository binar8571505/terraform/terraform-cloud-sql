provider "google" {
    project = "analog-medium-374702"
    region = "asia-southeast2"
    zone = "asia-southeast2-c"
}

provider "random" {
  
}

terraform {
    backend "gcs" {
        bucket = "binar-tf-state-syaifuddin"
        prefix = "project-terraform-sql"
    }
}