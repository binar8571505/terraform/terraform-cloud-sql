resource "random_id" "random_user_password" {
    byte_length = 12
}

resource "google_sql_user" "binar_user" {
    name = "people"
    password = random_id.random_user_password.hex
    instance = google_sql_database_instance.binar_sql_instance.name
}

resource "random_pet" "random_db_name" {
    length = 2
}

resource "google_sql_database" "db" {
    name = random_pet.random_db_name.id
    instance = google_sql_database_instance.binar_sql_instance.name
}
