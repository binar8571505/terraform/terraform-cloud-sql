output "binar_sql_user_name" {
    description = "Name of DB User"
    value = google_sql_user.binar_user.name
}

output "binar_sql_user_password" {
    description = "Password of DB User"
    value = random_id.random_user_password.hex
}

output "binar_sql_instance_name" {
    description = "Name of SQL instance"
    value = google_sql_database_instance.binar_sql_instance.name
}

output "binar_sql_instance_private_ip" {
    description = "Private IP of the SQL instance"
    value = google_sql_database_instance.binar_sql_instance.private_ip_address
}

output "binar_sql_instance_public_ip" {
    description = "Public IP of the SQL instance"
    value = google_sql_database_instance.binar_sql_instance.public_ip_address
}

output "binar_sql_db_name" {
    description = "Name of the database"
    value = random_pet.random_db_name.id
}