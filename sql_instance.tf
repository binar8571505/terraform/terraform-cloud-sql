resource "google_sql_database_instance" "binar_sql_instance" {
    name = "binar-sql"
    database_version = "MYSQL_8_0"
    settings {
      tier = "db-f1-micro"
      availability_type = "ZONAL"
      disk_size = 10
      activation_policy = "ALWAYS"
      deletion_protection_enabled = false
      ip_configuration {
        ipv4_enabled = true
        private_network = "projects/analog-medium-374702/global/networks/default"

        authorized_networks {
          name = "home"
          value = "125.166.13.117/32"
        }
      }
    }
}